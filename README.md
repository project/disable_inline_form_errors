## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers


## INTRODUCTION

The Disable Inline Form Errors module is an extension of the
Inline Form Errors module which is available in core. It gives the
flexibility to control the inline errors on the forms throughout the system,
by using the configuration where one can add the form id(s) and it's done.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/disable_inline_form_errors

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/disable_inline_form_errors


## REQUIREMENTS

No special requirements, just the Inline Form Errors module from core.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

 * No configurations.


## MAINTAINERS

Current maintainers:
 * Ravi Kumar Singh [rksyravi](https://www.drupal.org/u/rksyravi)
