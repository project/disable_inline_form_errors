<?php

namespace Drupal\disable_inline_form_errors\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements Class for DisableInlineFormErrors.
 */
class DisableInlineFormErrors extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'disable_inline_form_errors.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'disable_inline_form_errors_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['forms'] = [
      '#type' => 'textarea',
      '#rows' => 10,
      '#title' => $this->t('Form id(s)'),
      '#description' => $this->t("Enter one form_id per line. Leave blank if all forms."),
      '#default_value' => $config->get('forms'),
    ];

    $form['negate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negate the condition'),
      '#default_value' => $config->get('negate'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('forms', $form_state->getValue('forms'))
      ->set('negate', $form_state->getValue('negate'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
